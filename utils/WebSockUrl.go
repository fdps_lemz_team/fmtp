package utils

// url для web socker server
const (
	// URL для работы WS fmtp каналов
	ChannelURLPath = "/channels"
	// URL для работы с провадером AODB
	AodbURLPath = "/"
	// URL для работы с логгером
	LoggerURLPath = "/logger"
	// URL для web странички канала
	ChannelWebPath = "channel"
	// URL для web странички контроллера
	ChiefWebPath = "chief"
	// URL для web странички логгера
	LoggerWebPath = "logger"
)
